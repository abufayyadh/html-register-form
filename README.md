# PostTest Week 1

![example](output.png)
## Requirement

1. All fields cannot be blank, show alert if 1 of input are blank
2. Username length min. 3 char
3. Password length min. 6 char
4. Confirm password must same as Password
5. Email Validation using Regular Expression
6. Confirm Email must same as Email
7. After All form valid, show alert (Registeration Success) and let the field blank again
