const form = document.getElementById("form");
const userName = document.getElementById("username");
const password1 = document.getElementById("password1");
const password2 = document.getElementById("password2"); // mengambil element dg id form dr html
const email1 = document.getElementById("email1");
const email2 = document.getElementById("email2");

const inputs = document.querySelectorAll("input"); // mengambil lebih dari satu input
const alert = document.getElementById("alert");
let successPoint = 0; // untuk menampilkan kata berhasil, dan kosongin semua inputnya

form.addEventListener("submit", (e) => {
  // kalau kita klik submit, apapun masukan (klik, submit, dll)
  e.preventDefault(); // supaya dia tidak merefresh sendiri

  checkInputs(); // ini menjalankan function checkInputs, karena di JS ada hoisting
});

function checkInputs() {
  // trim to remove the whitespaces
  const usernameValue = userName.value.trim(); // value untuk mengambil apa yg ada didalam username, trim menghapus spasi
  const password1Value = password1.value.trim();
  const password2Value = password2.value.trim();
  const email1Value = email1.value.trim();
  const email2Value = email2.value.trim();

  if (usernameValue === "") {
    setErrorFor(userName, "Username cannot be blank");
  } else {
    if (usernameValue.length >= 3) {
      setSuccessFor(userName);
    } else {
      setErrorFor(
        userName,
        "Username cannot be less than 3 characters! Please reenter."
      );
    }
  }

  if (password1Value === "") {
    setErrorFor(password1, "Password cannot be blank");
  } else {
    if (password1Value.length >= 6) {
      setSuccessFor(password1);
    } else {
      setErrorFor(password1, "Minimum length for password is 6 characters!");
    }
  }

  if (password2Value === "") {
    setErrorFor(password2, "Passwords cannot be blank");
  } else if (password1Value !== password2Value) {
    setErrorFor(password2, "Passwords does not match");
  } else {
    if (password1Value.length >= 6) {
      setSuccessFor(password2);
    } else {
      setErrorFor(password2, "Minimum length for password is 6 characters!");
    }
  }

  if (email1Value === "") {
    setErrorFor(email1, "Email cannot be blank");
  } else if (!isEmail(email1Value)) {
    setErrorFor(email1, "Not a valid email");
  } else {
    setSuccessFor(email1);
  }

  if (email2Value === "") {
    setErrorFor(email2, "Email cannot be blank");
  } else if (email1Value !== email2Value) {
    setErrorFor(email2, "Email does not match");
  } else {
    setSuccessFor(email2);
  }
}

function setErrorFor(input, message) {
  const formControl = input.parentElement;
  const small = formControl.querySelector("small");
  formControl.className = "form-control error";
  small.innerText = message;
  successPoint = 0;
  alert.style.display = "none";
}

function setSuccessFor(input) {
  const formControl = input.parentElement;
  formControl.className = "form-control success";

  successPoint++;
  if (successPoint === 6) {
    // sesuai dengan jumlah inputan
    alert.style.display = "block";

    inputs.forEach(function (input) {
      input.value = "";
      input.parentElement.className = "form-control";
    });
    successPoint = 0;
  }
}

function isEmail(email) {
  return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
    email
  );
}
